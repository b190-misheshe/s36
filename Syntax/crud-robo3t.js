///// ********** 28. MongoDB - CRUD Operations  ********** /////

// *** CREATE (insert) *** //
db.users.insert({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
      phone: "87654321",
      email: "janedoe@gmail.com"
  },
  courses: [ "CSS", "Javascript", "Python" ],
  department: "none"
});

db.users.insertMany([
  {
      firstName: "Stephen",
      lastName: "Hawking",
      age: 76,
      contact: {
          phone: "87654321",
          email: "stephenhawking@gmail.com"
      },
      courses: [ "Python", "React", "PHP" ],
      department: "none"
  },
  {
      firstName: "Neil",
      lastName: "Armstrong",
      age: 82,
      contact: {
          phone: "87654321",
          email: "neilarmstrong@gmail.com"
      },
      courses: [ "React", "Laravel", "Sass" ],
      department: "none"
  }
]);

// *** READ (find) *** //
db.users.find();
db.users.find({ firstName: "Stephen" });
db.users.find({ firstName: "Stephen" }).pretty();
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();

// *** UPDATE (updateOne) *** //
db.users.updateOne(
  { firstName: "Test" },
  {
      $set : {
          firstName: "Bill",
          lastName: "Gates",
          age: 65,
          contact: {
              phone: "12345678",
              email: "bill@gmail.com"
          },
          courses: ["PHP", "Laravel", "HTML"],
          department: "Operations",
          status: "active"
      }
  }
);

// *** UPDATE (updateMany) *** //
db.users.find({ firstName: "Bill" }).pretty();

db.users.updateMany(
  { department: "none" },
  {
    $set: { department: "HR" }
  }
);

db.users.find().pretty();

// *** UPDATE (Replace) *** //
db.users.replaceOne(
  { firstName: "Bill" },
  {
      firstName: "Bill",
      lastName: "Gates",
      age: 65,
      contact: {
          phone: "12345678",
          email: "bill@gmail.com"
      },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations"
  }
);

db.users.find({ firstName: "Bill" }).pretty();

// *** DELETE *** //
db.users.insert({
  firstName: "test"
});

db.users.deleteOne({
  firstName: "test"
});

db.users.find().pretty();

// **** 29. MongoDB - Query Operators and Field Projection  *** //

// SECTION - Comparison Query Operators
db.users.find({age : { $gt : 50 }});
db.users.find({age : { $gte : 50 }});

db.users.find({age : { $lt : 50 }});
db.users.find({age : { $lte : 50 }});

db.users.find({age : { $ne : 82 }});

// specific match criteria on one field using different values
db.users.find({lastName : { $in : [ "Hawking", "Smith" ] }});
db.users.find({courses : { $in : [ "HTML", "React" ] }});

// SECTION - Logical Query Operators
db.users.find({$or : [ { firstName: "Neil" }, { age : 21 } ]});
db.users.find({$or: [ { firstName: "Neil" }, { age: { $gt: 21 } } ]});

db.users.find({$and: [ { age: { $ne : 82 } }, { age: { $ne : 76 } } ] });

// SECTION - Field Projection

// inclusion
db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// exclusion
db.users.find(
	{ firstName: "Jane" },
	{
		contact: 0,
		department: 0
	}
);

db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

