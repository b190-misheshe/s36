// contains functions and business logic of our express js application
// all operations it can do will be placed in this file
// model 

const Task = require("../models/task.js");

// defines the functions to be used in the "taskRoutes.js" 
module.exports.getAllTasks = () => {
  return Task.find({}).then(result => {
    return result;
  });
}

// function for creating a task
module.exports.createTask = (requestBody) => {
  let newTask = new Task ({
    name: requestBody.name
  });
  return newTask.save().then((task, error) => {
    if (error){
      console.log(error);
      //best practice to save resources
      return false;
    } else {
      return task;
    }
  })
}

// *** function for deleting a task
/* 
  Look for the task using "findByIdAndRemove" with the corresponding id provided/passed from the URI/route

  Delete the task
  -wait for the deletion to complete and then if there are any errors, print in the console the error
  -if there aer no errors, return the removed task

*/

module.exports.deleteTask = (taskId) => {
  // findByIdAndRemove - is a mongoose method that searches for the documents using the _id properties; after finding a document, the job of this method is to remove the document
  return Task.findByIdAndRemove(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return result
    }
  })
}

/*
module.exports.deleteTask = (requestID) => {
	let delTask = new Task({
		id: param.id
	});
	return delTask.save().then((del,error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return del;
		}
	})
}

module.exports.deleteTask = (requestBody) => {
  return Task.findByIdAndRemove(requestBody._id).then(result => {
    return result
  })
}

module.exports.deleteTask = (_id) => {
	return taskController.findByIdAndRemove({_id}).then((task,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			return id;
		};
	});
};
*/

// *** function for updating a task
// Business logic
/*
  1. get the task with the id using the mongoose "findById"
  2. replace the task's name returned from the database with the "name"

*/


module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result, error) => {
    if (error){
      console.log(error);
      return false;
    } else {
      result.name = newContent.name;
      return result.save().then((updatedTask, error) => {
        if (error) {
          console.log(error);
          return false
        } else {
          return updatedTask;
        }
      })
    }
  })
};

// ** ACTIVITY **
/*
Business Logic
1. Get the task with the id using the Mongoose method "findById"
*/

/*
Business Logic
1. Get the task with the id using the Mongoose method by "findById"
2. Change the status of the document to complete
3. Save the task
*/


module.exports.getTask = (taskId) => {
  return Task.findById(taskId).then(result => {
    return result;
  });
}; 

// module.exports.updatedStatus = (taskId, newStatus) => {
//   return Task.findById(taskId).then((result, error) => {
//     if (error){
//       console.log(error);
//       return false;
//     } else {
//       result.status = newStatus.status;
//       return result.save().then((updatedStatus, error) => {
//         if (error) {
//           console.log(error);
//           return false
//         } else {
//           return updatedStatus;
//         }
//       })
//     }
//   })
// };

module.exports.updateStatus = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error){
      console.log(error);
      return false;
    } else {
      result.status = "complete";
      return result.save().then((updatedStatus, error) => {
        if (error) {
          console.log(error);
          return false
        } else {
          return updatedStatus;
        }
      })
    }
  })
};