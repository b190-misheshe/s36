// ** BASIC IMPORTS

const express = require("express");
const mongoose = require("mongoose");

const taskRoutes = require("./routes/taskRoutes.js");

// ** SERVER SETUP
const app = express();

// ** MongoDB Connection
mongoose.connect("mongodb+srv://misheshe:admin@wdc028-course-booking.jj30hwt.mongodb.net/b190-to-do?retryWrites=true&w=majority",
{
  useNewUrlParser : true,
  useUnifiedTopology : true
}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));

app.use(express.json() );
app.use(express.urlencoded( { extended: true} ) );

// ******

// allows all the task routes created in the "taskRoutes.js" files to use "/task"
app.use("/tasks", taskRoutes);
// app.use("/users", taskUsers);























// ** SERVER RUNNING CONFIRMATION
app.listen(3000, () => console.log('Server is listening on port 3000'));