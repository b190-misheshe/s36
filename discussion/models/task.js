// model files should be import the mongoose module
// const { default: mongoose } = require("mongoose");
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema ({
  name: String, 
  status: {
    type: String, 
    default: "pending"
  }
});

// modules.exports = allow us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = mongoose.model("Task", taskSchema);
