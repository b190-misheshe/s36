// Contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server

// we have to use the Router method inside express
const express = require("express");

// allows us to access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// route to get all tasks
// get request to be sent at localhost:3000/tasks
// since the controller did not send any response, the task falls to the routes to send response that is received from the controllers and send it to the 
router.get("/", (req, res) => {
  taskController.getAllTasks()
  .then(resultFromController => res.send(resultFromController));
});

// miniactivity
// create "/" route that will process the request and send the response based on the createTask function inside the taskControllers.js

router.post("/", (req, res) => {
  taskController.createTask(req.body)
  .then(resultFromController => res.send(resultFromController));
});

// route for deleting a task
router.delete("/:id", (req, res) => {
  taskController.deleteTask(req.params.id)
  .then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req, res) => {
  taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// ** ACTIVITY **

router.get("/:id", (req, res) => {
  taskController.getTask(req.params.id)
  .then(resultFromController => res.send(resultFromController));
});

// router.patch("/:id", (req, res) => {
//   taskController.updatedStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
// });

router.put("/:id/complete", (req, res) => {
  taskController.updateStatus(req.params.id).
  then(resultFromController => res.send(resultFromController));
});

// allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = router;


